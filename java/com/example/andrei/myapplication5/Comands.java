package com.example.andrei.myapplication5;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andrei on 02.09.2015.
 */
public class Comands implements Parcelable {
    private String[] names = new String[2];
    private int score;
    private String commandName;
    private static  int count = 0;
    private int id;
    Comands(String[] names_) {
        id = count++;
        String[] comName = {"Команда1", "Команда2", "Команда3", "Команда4"};
        commandName = comName[id];
        for (int i = 0; i < names_.length; i++) names[i] = names_[i];
        score = 0;
    }

    public Comands(Parcel in) {
        Bundle bundle = in.readBundle();

        names = bundle.getStringArray("names");
        score = bundle.getInt("score");
        commandName = bundle.getString("commandName");
        count = bundle.getInt("count");
        id = bundle.getInt("id");
    }

    public static void setId(int value) {
        if (value < 0) throw new RuntimeException("Неправильный номер команды");
        count = value;
    }

    public void addScore(int scores) {
        score += scores;
    }

    public int getScore() {
        return score;
    }

    public static int getId() {
        return count;
    }

    public String getFistName() { return names[0]; }

    public  String getSecondName() { return names[1]; }

    @Override
    public String toString() {
        return commandName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Bundle bundle = new Bundle();
        bundle.putStringArray("names", names);
        bundle.putInt("score", score);
        bundle.putInt("id", id);
        bundle.putString("commandName", commandName);
        bundle.putInt("count", count);

        dest.writeBundle(bundle);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Comands createFromParcel(Parcel in) {
            return new Comands(in);
        }

        public Comands[] newArray(int size) {
            return new Comands[size];
        }
    };
}
