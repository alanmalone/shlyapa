package com.example.andrei.myapplication5;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


public class TwoactActivity extends Activity implements View.OnClickListener {
    TextView tvName;
    Spinner spWords;
    ListView lvComands;
    int rand;
    String[] Temp = new String[2];
    Button btnBegin;
    ArrayList<Comands> comands;
    ArrayList<String> names;
    //Bundle b = this.getIntent().getExtras();

    //ArrayAdapter<String>adapter=new ArrayAdapter<String>(getL)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.d("String", "ActivityTwo: onCreate()");
        setContentView(R.layout.twoact);

        comands = new ArrayList<Comands>(0);

        tvName = (TextView) findViewById(R.id.tvName);
        Intent intent = getIntent();
        names = intent.getStringArrayListExtra("mass");

        //Log.d("String", "create names");
        spWords = (Spinner) findViewById(R.id.spWords);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.words_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spWords.setAdapter(adapter);
        spWords.setPrompt("Количество слов");
        spWords.setSelection(0);

       // Log.d("String", "create spinner");
        btnBegin = (Button) findViewById(R.id.btnBegin);
        btnBegin.setOnClickListener(this);

        //Log.d("String", "set button onclick" + names.size());
        int j = 0;

        while (names.size() > 0) {
            rand = new Random().nextInt(names.size());
            //Log.d("String", String.valueOf(rand));
            Temp[0] = names.get(rand);
            //Log.d("String", Temp[0]);
            names.remove(rand);
            rand = new Random().nextInt(names.size());
            //Log.d("String", String.valueOf(rand));
            Temp[1] = names.get(rand);
            //Log.d("String", Temp[1]);
            names.remove(rand);
            comands.add(new Comands(Temp));
            //Log.d("String", comands.get(j).getFistName());
            j++;
        }

        //Log.d("String", "Comands size" + comands.size());

        //Log.d("String", "while is finish");
        final String ATTRIBUTE_NAME_TEAM = "team";
        final String ATTRIBUTE_NAME_FIRST = "first";
        final String ATTRIBUTE_NAME_LAST = "last"; //sony xperia t2

        lvComands = (ListView) findViewById(R.id.lvComands);
        ArrayList<Map<String, String>> data = new ArrayList<Map<String, String>>(comands.size());
        Map<String, String> m;
        for (int i = 0; i < comands.size(); i++) {
            m = new HashMap<String, String>();
            m.put(ATTRIBUTE_NAME_TEAM, comands.get(i).toString());
            m.put(ATTRIBUTE_NAME_FIRST, comands.get(i).getFistName());
            m.put(ATTRIBUTE_NAME_LAST, comands.get(i).getSecondName());
            data.add(m);
        }
        //Log.d("String", "create listview");
        String[] from = {ATTRIBUTE_NAME_TEAM, ATTRIBUTE_NAME_FIRST, ATTRIBUTE_NAME_LAST};
        int[] to = {R.id.tvTeamName, R.id.tvFirstName, R.id.tvLastName};

        SimpleAdapter sAdapter = new SimpleAdapter(this, data, R.layout.item, from, to);
        //ArrayAdapter<Comands> listComands = new ArrayAdapter<Comands>(this, android.R.layout.simple_list_item_2, comands);
        lvComands.setAdapter(sAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBegin:
                Intent intent = new Intent(this, ThreeActivity.class);
                intent.putExtra("mass", comands);
                intent.putExtra("words", spWords.getSelectedItem().toString());
                startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        comands.clear();
        Comands.setId(0);
        names.clear();
        //Log.d("String", "ActivityTwo: onDestroy()");
    }
    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_twoact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
