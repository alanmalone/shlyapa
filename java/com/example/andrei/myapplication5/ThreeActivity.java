package com.example.andrei.myapplication5;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class ThreeActivity extends Activity implements View.OnClickListener {

    String Temp[];
    TextView tvNameMember;
    TextView tvN1;
    TextView tvN2;
    EditText etWord;
    Button btnNext;
    Button btnEnterWord;
    int i = 0;
    int words;
    int memberNum = 1;
    int comandNum = 0;
    int endCycle;
    int count = 0;
    Intent intent;
    ArrayList<Comands> comands;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three);

        intent = getIntent();
        comands = getIntent().getParcelableArrayListExtra("mass");
        words = Integer.parseInt(intent.getStringExtra("words"));
        endCycle = 2*comands.size()*words;
        Temp = new String[endCycle];


        tvNameMember = (TextView) findViewById(R.id.tvNameMember);
        tvN1 = (TextView) findViewById(R.id.tvN1);
        tvN2 = (TextView) findViewById(R.id.tvN2);
        etWord = (EditText) findViewById(R.id.etWord);
        btnNext = (Button)findViewById(R.id.btnNext);
        btnEnterWord = (Button) findViewById(R.id.btnEnterWord);
        btnNext.setOnClickListener(this);
        btnEnterWord.setOnClickListener(this);
        tvNameMember.setText((CharSequence) comands.get(comandNum).getFistName());

        tvN2.setText(Integer.toString(words));
        tvN1.setText(Integer.toString(count));

        //tvText = (TextView) findViewById(R.id.tvText);
        //tvText.setText(String.valueOf(Integer.parseInt(intent.getStringExtra("words"))));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_three, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEnterWord:
                tvN1.setText(Integer.toString(++count));
                if (i == endCycle-1) {
                    Temp[i] = etWord.getText().toString();
                    count = 0;
                    tvN1.setText(Integer.toString(endCycle));
                    tvN2.setText(Integer.toString(endCycle));
                    AlertDialog.Builder builder = new AlertDialog.Builder(ThreeActivity.this);
                    builder.setTitle("Все готово")
                            .setMessage("Все слова введены, можете начинать первый этап")
                            .setCancelable(false)
                            .setNegativeButton("ОК",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                    btnNext.setEnabled(true);
                    btnEnterWord.setEnabled(false);
                    etWord.setText("");
                    etWord.setEnabled(false);
                } else {
                    if (i == words-1) {
                        if (memberNum%2 == 0) {
                            Temp[i] = etWord.getText().toString();
                            i++;
                            AlertDialog.Builder builder = new AlertDialog.Builder(ThreeActivity.this);
                            builder.setTitle("Следующий игрок")
                                    .setMessage("Теперь игрок " + comands.get(comandNum).getFistName() + " вводит слова")
                                    .setCancelable(false)
                                    .setNegativeButton("ОК",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                            count = 0;
                            tvN1.setText(Integer.toString(count));
                            tvNameMember.setText(comands.get(comandNum).getFistName());
                            memberNum++;
                            etWord.setText("");

                        } else {
                            Temp[i] = etWord.getText().toString();
                            i++;
                            AlertDialog.Builder builder = new AlertDialog.Builder(ThreeActivity.this);
                            builder.setTitle("Следующий игрок")
                                    .setMessage("Теперь игрок " + comands.get(comandNum).getSecondName() + " вводит слова")
                                    .setCancelable(false)
                                    .setNegativeButton("ОК",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                            count = 0;
                            tvN1.setText(Integer.toString(count));
                            tvNameMember.setText(comands.get(comandNum).getSecondName());
                            memberNum++;
                            comandNum++;
                            etWord.setText("");
                        }
                        words += Integer.parseInt(intent.getStringExtra("words"));
                    } else {
                        Temp[i] = etWord.getText().toString();
                        i++;
                        etWord.setText("");
                    }
                }
                break;
            case R.id.btnNext:
                int comN =comands.size();
                Intent intent = new Intent(this, FourActivity.class);
                intent.putExtra("mass", comands);
                intent.putExtra("words", Temp);
                intent.putExtra("numWords",endCycle);
                intent.putExtra("comNumb",comN);
                startActivity(intent);

        }
    }

}
