package com.example.andrei.myapplication5;

import java.util.Random;

/**
 * Created by Andrei on 29.08.2015.
 */
public class WheelOfFortune {
    private String[] words;
    private int random;
    WheelOfFortune(String[] words_, int length) {
        if (length == 0) throw new RuntimeException("Не введено ни одного слова");
        random = new Random().nextInt(length);
        words = new String[length];
        for (int i = 0; i < length; i++) words[i] = words_[i];
    }

    public String giveAWord() {
        return words[random];
    }
    public int getRandom() { return  random; }
}
