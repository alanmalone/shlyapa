package com.example.andrei.myapplication5;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SizeF;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;


public class FourActivity extends Activity implements View.OnClickListener {

    TextView tvTimer;
    TextView tvTeamName;
    TextView tvPlayerName;
    Button btnStart;
    Button btnWords;
    MyTimer timer;
    boolean flag=false;
    Intent intent;
    ArrayList<Comands> comand;
    ArrayList<String> Player;
    String Temp[];

    int playerCounter=0,comCounter=0;
    int counterW=0;
    int end,comNumb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_four);

        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvTeamName = (TextView) findViewById(R.id.tvTeamName);
        tvPlayerName = (TextView) findViewById(R.id.tvPlayerName);
        btnStart=(Button)findViewById(R.id.btnStart);
        btnStart.setOnClickListener(this);
        btnWords = (Button)findViewById(R.id.btnWords);
        btnWords.setOnClickListener(this);
        intent = getIntent();
        comand = getIntent().getParcelableArrayListExtra("mass");
        Player = getIntent().getStringArrayListExtra("Key");
        Temp = getIntent().getStringArrayExtra("words");
        end = getIntent().getIntExtra("numWords", 0);
        comNumb = getIntent().getIntExtra("comNumb", 0);

        tvTeamName.setText(comand.get(comCounter).toString());
        tvPlayerName.setText(comand.get(playerCounter).getFistName());
        timer = new MyTimer(30000, 1000);
        timer.onTick(30000);
        //mixString(Temp);
    }

    public void mixString(String T[]){
        Random rand = new Random();
        String Buf;
        int size=T.length,r;
        for(int n=0;n<size;n++){
            Buf=Temp[n];
            r = n + (int)(Math.random() * ((size - n) + 1));
            Temp[n] = Temp[r];
            Temp[r] = Buf;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_four, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnStart:

                if(flag==false) {
                    timer.start();
                    btnWords.setText(Temp[counterW]);
                    if (counterW<=end) {
                        counterW++;
                    }else{
                        Log.d("String", "onBreK");
                        break;
                    }
                    btnStart.setText("Закончить");
                    flag=true;
                    Log.d("String", String.valueOf(counterW));
                } else {
                    if(playerCounter == (comand.size()*2)-1){
                        playerCounter=0;
                        comCounter=0;
                        flag=false;
                        tvTeamName.setText(comand.get(comCounter).toString());
                        tvPlayerName.setText(comand.get(comCounter).getFistName());
                        Log.d("String", "onCircle");
                    }
                    if(flag==true) {
                        timer.cancel();
                        timer.onTick(30000);
                        btnStart.setText("Начать");
                        flag = false;
                        Log.d("String", "onEndGame");
                        if (playerCounter % 2 == 0) {
                            tvPlayerName.setText(comand.get(comCounter).getSecondName());
                            Log.d("String", "ChangeGamer");
                            playerCounter++;
                        } else {
                            comCounter++;
                            tvTeamName.setText(comand.get(comCounter).toString());
                            tvPlayerName.setText(comand.get(comCounter).getFistName());
                            Log.d("String", "ChangeComand");
                            playerCounter++;
                        }
                    }
                    Log.d("String", String.valueOf(counterW));
                }
                break;
            case R.id.btnWords:
                if(flag==true) {
                    if (counterW<=end) {
                        btnWords.setText(Temp[counterW]);
                        counterW++;
                        comand.get(comCounter).addScore(1);
                    }else{break;}
                }
                break;
        }
    }

    public class MyTimer extends CountDownTimer {
        public MyTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            // TODO Auto-generated constructor stub
            tvTimer.setText("");
        }

        @Override
        public void onFinish() {
            // TODO Auto-generated method stub
            tvTimer.setText("Время истекло");
            btnStart.setText("Начать");
            flag=true;
            comCounter++;
            if(playerCounter==0){
                tvPlayerName.setText(comand.get(comCounter).getSecondName());
                playerCounter++;
            }else{
                comCounter++;
                tvTeamName.setText(comand.get(comCounter).toString());
                tvPlayerName.setText(comand.get(comCounter).getFistName());
                playerCounter--;
            }
        }

        @Override
        public void onTick(long millisUntilFinished) {
            // TODO Auto-generated method stub
            tvTimer.setText("Время:\t" + millisUntilFinished / 1000);
        }

       /* @Override
        public void onPause() {
            super.onPause();
            timer.cancel();
            timer.purge();
            h2.removeCallbacks(run);
            Button b = (Button)findViewById(R.id.button);
            b.setText("start");
        }*/
    }
}
