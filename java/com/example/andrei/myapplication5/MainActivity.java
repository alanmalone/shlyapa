package com.example.andrei.myapplication5;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;
import java.util.ArrayList;
import android.content.Context;


public class MainActivity extends Activity implements View.OnClickListener {

    Button btnLastDelete;
    TextView tvNumber;
    Button btnEnter;
    Button btnDistribute;
    EditText etName;
    ArrayList<String> ComNam = new ArrayList<String>(0);
    int number = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.firstact);

        btnDistribute = (Button) findViewById(R.id.btnDistribute);
        btnLastDelete = (Button) findViewById(R.id.btnLastDelete);
        btnEnter = (Button) findViewById(R.id.btnEnter);
        btnDistribute.setOnClickListener(this);
        btnEnter.setOnClickListener(this);
        btnLastDelete.setOnClickListener(this);
        etName = (EditText) findViewById(R.id.etName);
        tvNumber = (TextView) findViewById(R.id.tvNumber);
        tvNumber.setText(String.valueOf(number));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEnter:
                if (!etName.getText().toString().equals("")) {
                    ComNam.add(number, etName.getText().toString());
                    number++;
                    etName.setText("");
                    tvNumber.setText(String.valueOf(number));
                } else {
                    Toast.makeText(this, "Ничего не введено, повторите ввод", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnDistribute:
                if ((ComNam.size() < 4) || (ComNam.size() % 2 != 0)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Ошибка")
                            .setMessage("Нужно ввести чётное количество игроков")
                            .setCancelable(false)
                            .setNegativeButton("ОК",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                    break;
                }
                Intent intentName = new Intent(this, FourActivity.class);
                Intent intent = new Intent(this, TwoactActivity.class);
                intent.putExtra("mass", ComNam);
                intentName.putExtra("Key",ComNam);
                startActivity(intent);
                break;
            case R.id.btnLastDelete:
                if (ComNam.size() != 0) {
                    ComNam.remove(number-1);
                    number--;
                } else {
                    Toast.makeText(this, "Нечего удалять", Toast.LENGTH_LONG).show();
                }
                tvNumber.setText(String.valueOf(number));
        }
    };


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }
}
